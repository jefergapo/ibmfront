import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { logging } from 'protractor';
import { tap, map } from 'rxjs/operators';

@Injectable()
export class AdviserService {

  advisersEndpoint = 'http://localhost:8080/advisers'

  loggingEndPoint = 'http://localhost:8080/login'

  constructor(private http: HttpClient) { }

  getAllAdvisers(): Observable<any[]> {
    return this.http.get<any[]>(this.advisersEndpoint);
  }

  save(client: any): Observable<any> {
    return this.http.post<any[]>(this.advisersEndpoint, client);
  }

  delete(id: string): Observable<any[]> {
    return this.http.delete<any[]>(`${this.advisersEndpoint}/${id}`);
  }

  loging(user: any) {
    return this.http.post<any[]>(this.loggingEndPoint, user, {observe : 'response'}).pipe(
      map(authenticateSuccess)
    );

    function authenticateSuccess(resp) {
      const bearerToken = resp.headers.get('Authorization');
      if (bearerToken && bearerToken.slice(0, 7) === 'Bearer ') {
          const jwt = bearerToken.slice(7, bearerToken.length);
          return jwt;
      }
  }
  }
        

}
