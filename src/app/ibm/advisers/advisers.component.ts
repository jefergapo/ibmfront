import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AdviserService } from './services/adviser.service';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-advisers',
  templateUrl: './advisers.component.html',
  styleUrls: ['./advisers.component.css']
})
export class AdvisersComponent implements OnInit {

  advisers: any;

  form: FormGroup

  constructor(
    private adviserService: AdviserService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.getAdvisers();
    this.form = this.fb.group({
      id: [''],
      name: ['', Validators.required],
      specialty: ['', Validators.required],
      user: this.fb.group({
        username: ['', Validators.required],
        password: ['', Validators.required]
      })
    })
  }

  getAdvisers() {
    this.adviserService.getAllAdvisers().pipe(
      tap(advisers => this.advisers = advisers)
    ).subscribe(resp =>{
      console.log(resp);
    })
  }

  edit(client: any): void {
    this.form.patchValue(client);
  }

  cleanForm() {
    this.form.reset();
  }

  saveAdviser() {
    if (this.form.valid) {
      this.adviserService.save(this.form.value).pipe(
        tap( x => this.getAdvisers() )
      ).subscribe(x => {
        console.log(x)
      })
    } else {
      console.log('[form invalido ]')
    }
  }

  deleteAdviser(id: string) {
    this.adviserService.delete(id).pipe(
      tap( x => this.getAdvisers() )
    ).subscribe(x => {
      console.log(x)
    })
  }

}
