import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal, NgbDateAdapter, NgbDateParserFormatter,  } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CardHistoryService } from './services/card-history.service';
import { CardService } from '../cards/services/card.service';
import { Observable } from 'rxjs';
import { tap, mergeMap } from 'rxjs/operators';

@Component({
  selector: 'app-card-history',
  templateUrl: './card-history.component.html',
  styleUrls: ['./card-history.component.css']
})
export class CardHistoryComponent implements OnInit {

  @Input()
  card: any;

  model2:any;

  cardHistory: Date[];

  form: FormGroup;

  constructor(
    private cardHistoryService: CardHistoryService,
    public activeModal: NgbActiveModal,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.buildForm();
    this.getCardHistoryByCard(this.card.id).subscribe(_ => _)
  }

  getCardHistoryByCard(id: string): Observable<any>  {
    return this.cardHistoryService.getCardHistoryByCardId(id).pipe(
      tap(cardHistory =>  this.cardHistory = cardHistory),
      tap(cardHistory =>  console.log(cardHistory)),
    )
  }

  buildForm() {
    this.form = this.fb.group({
      id: [''],
      transactionDate: ['', Validators.required],
      description: ['', Validators.required],
      amount: ['', Validators.required],
    })
  }

  editCardConsume(cardConsume: any): void {
    this.form.patchValue(this.mapDateToForm(cardConsume));
  }

  mapDateToForm(cardConsume: any) {
    return {
      ...cardConsume,
      transactionDate: {
        year: new Date(2012, 9, 10).getFullYear,
        month: new Date(2012, 9, 10).getMonth,
        day: new Date(2012, 9, 10).getDay
      }
    }
  }

  mapDateToModel(cardConsumeForm: any) {
    return {
      ...cardConsumeForm,
      transactionDate: new Date(cardConsumeForm.transactionDate.year,
                                cardConsumeForm.transactionDate.month,
                                cardConsumeForm.transactionDate.day
                                )
    }
  }

  cleanForm() {
    this.form.reset();
  }

  generateTransaction() {
    if (this.form.valid) {
      const tx = { ...this.mapDateToModel(this.form.value), cardId: this.card.id };
      this.cardHistoryService.save(tx).pipe(
        mergeMap(tx => this.getCardHistoryByCard(this.card.id))
      ).subscribe(x => {
        this.form.reset();
        console.log(x)
      })
    } else {
      console.log('[form invalido ]')
    }
  }

  deleteCardConsume(id: string) {
    this.cardHistoryService.delete(id).pipe(
      mergeMap( x => this.getCardHistoryByCard(this.card.id) )
    ).subscribe(x => {
      console.log(x)
    })
  }

}
