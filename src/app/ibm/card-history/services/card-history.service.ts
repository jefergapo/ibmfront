import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class CardHistoryService {
  
  cardsEndpoint = 'http://localhost:8080/card-transactions'
  cardsByClientEndpoint = 'http://localhost:8080/cards/:id/card-transactions'


  constructor(private http: HttpClient) { }

  getCardHistoryByCardId(id: string): Observable<any[]> {
    return this.http.get<any[]>(this.cardsByClientEndpoint.replace(':id', id));
  }

  save(client: any): Observable<any[]> {
    return this.http.post<any[]>(this.cardsEndpoint, client);
  }

  delete(id: string): Observable<any[]> {
    return this.http.delete<any[]>(`${this.cardsEndpoint}/${id}`);
  }

}
