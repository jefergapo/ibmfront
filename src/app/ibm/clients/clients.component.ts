import { Component, OnInit } from '@angular/core';
import { ClientService } from './services/client.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {

  clients: any;

  form: FormGroup

  constructor(
    private clientService: ClientService,
    private fb: FormBuilder,
    private router: Router
  ) { }

  ngOnInit() {
    this.getClients();
    this.form = this.fb.group({
      id: [''],
      name: ['', Validators.required],
      addres: ['', Validators.required],
      city: ['', Validators.required],
      phoneNumber: ['', Validators.required],
    })
  }

  getClients() {
    this.clientService.getAllClients().pipe(
      tap(x => this.clients = x)
    ).subscribe(resp =>{
      console.log(resp);
    })
  }

  edit(client: any): void {
    this.form.patchValue(client);
  }

  cleanForm() {
    this.form.reset();
  }

  goToCards(client: any) {
    this.router.navigate([`tarjetas/${client.id}`]);
  }

  saveClient() {
    if (this.form.valid) {
      this.clientService.save(this.form.value).pipe(
        tap( x => this.getClients() )
      ).subscribe(x => {
        console.log(x)
      })
    } else {
      console.log('[form invalido ]')
    }
  }

  deleteClient(id: string) {
    this.clientService.delete(id).pipe(
      tap( x => this.getClients() )
    ).subscribe(x => {
      console.log(x)
    })
  }

}
