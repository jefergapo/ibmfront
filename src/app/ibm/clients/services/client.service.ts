import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ClientService {

  clientsEndpoint = 'http://localhost:8080/clients'

  constructor(private http: HttpClient) { }

  getAllClients(): Observable<any[]> {
    return this.http.get<any[]>(this.clientsEndpoint);
  }

  getClientById(id: string): Observable<any> {
    return this.http.get<any[]>(`${this.clientsEndpoint}/${id}`);
  }

  save(client: any): Observable<any[]> {
    return this.http.post<any[]>(this.clientsEndpoint, client);
  }

  delete(id: string): Observable<any[]> {
    return this.http.delete<any[]>(`${this.clientsEndpoint}/${id}`);
  }

}
