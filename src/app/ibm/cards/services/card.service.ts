import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CardService {

  cardsEndpoint = 'http://localhost:8080/cards'
  cardsByClientEndpoint = 'http://localhost:8080/clients/:id/cards'


  constructor(private http: HttpClient) { }

  getCardsByClient(id: string): Observable<any[]> {
    return this.http.get<any[]>(this.cardsByClientEndpoint.replace(':id', id));
  }

  save(client: any): Observable<any[]> {
    return this.http.post<any[]>(this.cardsEndpoint, client);
  }

  delete(id: string): Observable<any[]> {
    return this.http.delete<any[]>(`${this.cardsEndpoint}/${id}`);
  }

}
