import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { CardService } from './services/card.service';
import { mergeMap, tap, map, catchError } from 'rxjs/operators';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ClientService } from '../clients/services/client.service';
import { Observable } from 'rxjs/Observable';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { CardHistoryComponent } from '../card-history/card-history.component';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {

  cards: any[];
  client: any;

  form: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private cardService: CardService,
    private clientService: ClientService,
    private fb: FormBuilder,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.buildForm();
    this.getIdClientFromRoute();
  }

  openModal(card: any) {
    const modalRef = this.modalService.open(CardHistoryComponent, {size: 'lg'});
    modalRef.componentInstance.card = card;
    modalRef.result.then(x => console.log(x))
  }

  getIdClientFromRoute(): void {
    this.route.params.pipe(
      mergeMap( (param: Params) => this.initQuery(param.id)),
    ).subscribe(x => {
      console.log(x)
    }, e => {
      this.goToClients()
    });
  }

  initQuery(id): Observable<any> {
    return forkJoin(
      this.getClientById(id),
      this.getCardsByClient(id)
    )
  }

  getCardsByClient(id: string): Observable<any>  {
    return this.cardService.getCardsByClient(id).pipe(
      tap(cards =>  this.cards = cards),
    )
  }

  getClientById(id: string): Observable<any> {
    return this.clientService.getClientById(id).pipe(
      tap(client =>  this.client = client),
    )
  }

  buildForm() {
    this.form = this.fb.group({
      id: [''],
      number: ['', Validators.required],
      ccv: ['', Validators.required],
      type: ['', Validators.required],
    })
  }

  editCard(card: any): void {
    this.form.patchValue(card);
  }

  cleanForm() {
    this.form.reset();
  }

  goToCardHistory(card: any) {
    this.router.navigate([`consumos/${card.id}`]);
  }

  goToClients() {
    this.router.navigate(['clientes']);
  }

  saveCard() {
    if (this.form.valid) {
      const card = { ...this.form.value, clientId: this.client.id };
      this.cardService.save(card).pipe(
        mergeMap(card => this.getCardsByClient(this.client.id))
      ).subscribe(x => {
        this.form.reset();
        console.log(x)
      })
    } else {
      console.log('[form invalido ]')
    }
  }

  deleteCard(id: string) {
    this.cardService.delete(id).pipe(
      mergeMap( x => this.getCardsByClient(this.client.id) )
    ).subscribe(x => {
      console.log(x)
    })
  }

}
