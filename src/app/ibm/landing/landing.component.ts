import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AdviserService } from "../advisers/services/adviser.service";


@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {
  
    advisers: any;
  
    form: FormGroup
  
    constructor(
      private adviserService: AdviserService,
      private fb: FormBuilder
    ) { }
  
    ngOnInit() {
      this.form = this.fb.group({
        id: [''],
        name: ['', Validators.required],
        specialty: ['', Validators.required],
        user: this.fb.group({
          username: ['', Validators.required],
          password: ['', Validators.required]
        })
      })
    }
  
    cleanForm() {
      this.form.reset();
    }
  
    saveAdviser() {
      if (this.form.valid) {
        this.adviserService.save(this.form.value).subscribe(x => {
          console.log(x)
        })
      } else {
        console.log('[form invalido ]')
      }
    }
  
  }
  