import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientsComponent } from './clients/clients.component';
import { AdvisersComponent } from './advisers/advisers.component';
import { CardsComponent } from './cards/cards.component';
import { RouterModule } from '@angular/router';
import { CardHistoryComponent } from './card-history/card-history.component';
import { ClientService } from './clients/services/client.service';
import { ReactiveFormsModule } from '@angular/forms';
import { CardService } from './cards/services/card.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CardHistoryService } from './card-history/services/card-history.service';
import { AdviserService } from './advisers/services/adviser.service';
import { LandingComponent } from './landing/landing.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
  ],
  providers: [
    ClientService,
    CardService,
    CardHistoryService,
    AdviserService
  ],
  declarations: [
    ClientsComponent,
    AdvisersComponent,
    CardsComponent,
    CardHistoryComponent,
    LandingComponent
  ],
  entryComponents: [CardHistoryComponent]
})
export class IbmModule { }
