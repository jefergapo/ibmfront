export class Session {
  public token: string;
  public user: User;
}

export class User {
  public name: string;
  public username: User;
}