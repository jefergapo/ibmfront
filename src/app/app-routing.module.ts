import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientsComponent } from './ibm/clients/clients.component';
import { AdvisersComponent } from './ibm/advisers/advisers.component';
import { CardsComponent } from './ibm/cards/cards.component';
import { LandingComponent } from './ibm/landing/landing.component';

const routes: Routes = [
  { path: '', redirectTo: 'asesores', pathMatch: 'prefix' },
  { 
    path: 'clientes', component: ClientsComponent
  },
  { 
    path: 'tarjetas/:id', component: CardsComponent
  },
  { path: 'asesores', component: AdvisersComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }