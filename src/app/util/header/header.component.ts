import { Component, OnInit } from '@angular/core';
import { AdviserService } from '../../ibm/advisers/services/adviser.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  form: FormGroup;

  constructor(
    private adviserService: AdviserService,
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  login() {
    if (this.form.valid){
      this.adviserService.loging(this.form.value).subscribe(response => {
        console.log(response)
      })
    } else {
      console.log("formulario invalido")
    }
  }

}
