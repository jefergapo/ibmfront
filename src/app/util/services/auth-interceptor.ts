import { Observable } from 'rxjs/Observable';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
 
export class AuthInterceptor implements HttpInterceptor {
 
    constructor(
    ) {
    }
 
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        
        if (!request || !request.url ) {
            return next.handle(request);
        }
 
        const token = localStorage.getItem('Authentication')
        if (!!token) {
            request = request.clone({
                setHeaders: {
                    Authorization: token
                }
            });
        }
        return next.handle(request);
    }
 
}